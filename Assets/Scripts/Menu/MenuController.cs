using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// -------- WORK MADE BY MATHEUS FRANÇA
public class MenuController : MonoBehaviour
{
    //Loads a scene using its name
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }

    //Leaves the application
    public void ExitGame()
    {
        Application.Quit();
    }
}
