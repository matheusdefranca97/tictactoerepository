using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

// -------- WORK MADE BY MATHEUS FRANÇA
public class MenuAnimations : MonoBehaviour
{
    [Header("Canvas GameObjects")]
    [SerializeField] GameObject headerText;
    [SerializeField] GameObject playButton, exitButton, highScoreButton;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(FirstMenuAnimation());
    }


    //the movements that the gameobjects will do when the menu starts
    IEnumerator FirstMenuAnimation()
    {
        //where to move the gameObjects
        Vector3 whereToHeaderText = new Vector3(0, 1400, 0);
        Vector3 whereToPlayButton = Vector3.zero;
        Vector3 whereToExitButton = new Vector3(0, -800, 0);
        Vector3 whereToHighScoreButton = new Vector3(0, -400, 0);

        //move the gameobjects to that locations with a 1 second duration and a gap of 0.5 seconds between them
        headerText.transform.DOLocalMove(whereToHeaderText, 1f);
        playButton.transform.DOLocalMove(whereToPlayButton, 1f);
        yield return new WaitForSeconds(0.5f);
        highScoreButton.transform.DOLocalMove(whereToHighScoreButton, 1f);
        yield return new WaitForSeconds(0.5f);
        exitButton.transform.DOLocalMove(whereToExitButton, 1f);


    }



}
