using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// -------- WORK MADE BY MATHEUS FRANÇA
public class BoardController : MonoBehaviour
{
    public List<SpotApplication> boardSpots;


    //all the rows positions where player/enemy can win if they take over the three of them
    int[,] winPositions = new int[8, 3] { { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 }, { 0, 3, 6 },
    { 1, 4, 7 }, { 2, 5, 8 }, { 0, 4, 8 }, { 2, 4, 6 } };

    //variable that maps the position in the board where the player/enemy is close to win
    int closeToWinPosition;

    //variable that helps the count for draw games
    int tieCounter = 0;



    //check if someone won the game
    //for the player or enemy, I have declared the matrix "winPositions", that map all the possible wins spots that both can have.
    //every time the player or enemy makes a move, I pass through all those "winPositions" to check if all the identifiers of one "winPositions" index belongs to the player or the enemy, so I can be sure one of them won the game
    //i have a tieCounter too, every time I pass through the function the tieCounter increases by 1, so when he reaches 9, i know the game is over in a tie, since theres no empty spot available to play and neither the player or the enemy won the game.
    public string SomeoneWin()
    {
        tieCounter++;
        for (int i = 0; i < winPositions.GetLength(0); i++)
        {
            if (boardSpots[winPositions[i, 0]].model.myIdentifier == SpotModel.Identifier.Player &&
            boardSpots[winPositions[i, 1]].model.myIdentifier == SpotModel.Identifier.Player &&
            boardSpots[winPositions[i, 2]].model.myIdentifier == SpotModel.Identifier.Player)
            {
                return "Player";
            }
            else if (boardSpots[winPositions[i, 0]].model.myIdentifier == SpotModel.Identifier.Enemy &&
           boardSpots[winPositions[i, 1]].model.myIdentifier == SpotModel.Identifier.Enemy &&
           boardSpots[winPositions[i, 2]].model.myIdentifier == SpotModel.Identifier.Enemy
           )
            {

                return "Enemy";
            }

        }
        if (tieCounter >= 9)
        {
            return "Draw";
        }
        return "Nobody";
    }

    //function that returns if a spot is empty or have been already taken
    public bool PositionIsEmpty(int position)
    {
        if (boardSpots[position].model.myIdentifier == SpotModel.Identifier.Nobody)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //check if someone is close to win
    //for the player or enemy, I have declared the matrix "winPositions", that map all the possible wins spots that both can have.

    //every time the player or enemy makes a move, I pass through all those "winPositions" to check if two of the three identifiers in a "winPositions" index belongs to the player or the enemy,
    //the third one must belongs to nobody, so i can identify that spot as a close to win position
    public bool SomeoneIsCloseToWin(SpotModel.Identifier identifierToCheck)
    {
        for (int i = 0; i < winPositions.GetLength(0); i++)
        {

            if (boardSpots[winPositions[i, 0]].model.myIdentifier == identifierToCheck &&
            boardSpots[winPositions[i, 1]].model.myIdentifier == identifierToCheck
            && boardSpots[winPositions[i, 2]].model.myIdentifier == SpotModel.Identifier.Nobody
            )
            {
                closeToWinPosition = winPositions[i, 2];
                return true;
            }


            else if (
            boardSpots[winPositions[i, 0]].model.myIdentifier == identifierToCheck &&
            boardSpots[winPositions[i, 2]].model.myIdentifier == identifierToCheck
            && boardSpots[winPositions[i, 1]].model.myIdentifier == SpotModel.Identifier.Nobody)
            {
                closeToWinPosition = winPositions[i, 1];
                return true;
            }


            else if (
            boardSpots[winPositions[i, 1]].model.myIdentifier == identifierToCheck &&
            boardSpots[winPositions[i, 2]].model.myIdentifier == identifierToCheck
            && boardSpots[winPositions[i, 0]].model.myIdentifier == SpotModel.Identifier.Nobody)

            {
                closeToWinPosition = winPositions[i, 0];
                return true;
            }
        }

        return false;
    }


    //returns the spot found on the SomeoneIsCloseToWin function
    public SpotApplication GetCloseToWinBoardSpot()
    {
        return boardSpots[closeToWinPosition];
    }




}
