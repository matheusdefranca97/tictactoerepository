using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// -------- WORK MADE BY MATHEUS FRANÇA
public class SpotController : MonoBehaviour
{
    SpotApplication myApp;
    GameController myGameController;
    private void Awake()
    {
        myApp = GetComponent<SpotApplication>();
        myGameController = FindObjectOfType<GameController>();
    }
    // Start is called before the first frame update
    // Add the function ConfigurateAfterClick into this Spot button
    void Start()
    {
        myApp.view.myButton.onClick.AddListener(() => ConfigurateAfterClick());
    }



    // configures the spot button after the player/enemy clicks it
    public void ConfigurateAfterClick()
    {
        if (myGameController.GetCurrentTurn() == Turn.PlayerTurn)
        {
            PlayerClickBoardButton();
        }
        else
        {
            EnemyClickBoardButton();
        }


    }

    //if the player clicks this spot, transform this Spot text into an "O", which represents the player symbol, change the spot identifier to know this place belongs to a player, and also tell my GameController that player turn is over.
    public void PlayerClickBoardButton()
    {

        myApp.view.myText.text = "O";
        myApp.model.myIdentifier = SpotModel.Identifier.Player;
        myGameController.EndTurn();
        Destroy(myApp.view.myButton);
    }

    //if the enemy clicks this spot, transform this Spot text into an "X" and change its color to orange, which represents the enemy symbol, change the spot identifier to know this place belongs to an enemy, and also tell my GameController that player turn is over.
    public void EnemyClickBoardButton()
    {
        myApp.view.myColor = new Color32(255, 94, 0, 255);
        myApp.view.myText.color = myApp.view.myColor;
        myApp.view.myText.text = "X";
        myApp.model.myIdentifier = SpotModel.Identifier.Enemy;
        myGameController.EndTurn();
        Destroy(myApp.view.myButton);
    }


    //Turn this spot button off, so nobody can click on it
    public void TurnOnOffButton(bool value)
    {
        myApp.view.myButton.enabled = value;
    }
}
