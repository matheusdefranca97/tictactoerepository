using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// -------- WORK MADE BY MATHEUS FRANÇA
public class SpotView : MonoBehaviour
{
    [HideInInspector] public Button myButton;
    [HideInInspector] public Text myText;
    [HideInInspector] public Color myColor;

    private void Awake()
    {
        myButton = GetComponent<Button>();
        myText = GetComponentInChildren<Text>();

    }


}
