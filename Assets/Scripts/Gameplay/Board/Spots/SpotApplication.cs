using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// -------- WORK MADE BY MATHEUS FRANÇA
public class SpotApplication : MonoBehaviour
{
    [HideInInspector] public SpotController controller;
    [HideInInspector] public SpotModel model;
    [HideInInspector] public SpotView view;





    private void Awake()
    {
        controller = GetComponent<SpotController>();
        model = GetComponent<SpotModel>();
        view = GetComponent<SpotView>();
    }
}
