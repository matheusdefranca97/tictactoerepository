using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// -------- WORK MADE BY MATHEUS FRANÇA
public class SpotModel : MonoBehaviour
{
    [HideInInspector]
    public enum Identifier
    {
        Nobody,
        Player,
        Enemy
    };

    //check if my spot belongs to player or enemy

    public Identifier myIdentifier;
}
