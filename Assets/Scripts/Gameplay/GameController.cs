
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

// -------- WORK MADE BY MATHEUS FRANÇA
public class GameController : MonoBehaviour
{

    GameDifficulty currentGameDifficulty;
    Turn currentTurn;
    [Header("Scripts References")]
    [SerializeField] BoardController myBoardController;
    [SerializeField] EnemyApplication enemy;
    [SerializeField] GameplayAnimations gameplayAnimations;
    [Header("UI References")]
    [SerializeField] TextMeshProUGUI stateText;
    bool gameIsOver;

    #region GET
    //returns my current turn
    public Turn GetCurrentTurn()
    {
        return currentTurn;
    }

    //returns whats my current difficulty is
    public GameDifficulty GetCurrentGameDifficulty()
    {
        return currentGameDifficulty;
    }

    #endregion
    #region START GAME
    //choose the current game difficulty according to the button im clicking
    public void ClickDifficultyButton(string difficulty)
    {
        switch (difficulty)
        {
            case "Easy":
                currentGameDifficulty = GameDifficulty.Easy;
                break;
            case "Medium":
                currentGameDifficulty = GameDifficulty.Medium;
                break;
            case "Hard":
                currentGameDifficulty = GameDifficulty.Hard;
                break;
            default:
                currentGameDifficulty = GameDifficulty.Easy;
                break;
        }
    }





    //randomly chooses who starting, with a 50% chance for both player and enemy
    public void RandomlyPickWhoStarts()
    {

        if (Random.value < 0.5f)
        {
            currentTurn = Turn.PlayerTurn;
        }
        else
        {
            currentTurn = Turn.EnemyTurn;
            enemy.controller.CallChooseMove();
        }
    }

    #endregion
    #region TURN RELATED
    //a function called after the player or enemy makes their move, and checks with the board if the game is over and someone has won or if it's a draw, otherwise continues the game
    public void EndTurn()
    {
        string whoWon = myBoardController.SomeoneWin();
        switch (whoWon)
        {
            case "Nobody":
                PassTurn();
                break;
            case "Player":
                PlayerWin();
                break;
            case "Enemy":
                EnemyWin();
                break;
            case "Draw":
                DrawGame();
                break;

        }
    }


    //if nobody won, this function is called to pass the turn and change its state between the player/enemy
    void PassTurn()
    {

        if (currentTurn == Turn.EnemyTurn)
        {
            currentTurn = Turn.PlayerTurn;
            TurnAllButtonsOnOff(true);
        }
        else
        {

            currentTurn = Turn.EnemyTurn;
            enemy.controller.CallChooseMove();
        }
    }

    #endregion
    #region END GAME
    //what will commonly happen when the game is over, no matter if its a tie, a player win or an enemy win
    void GameOver()
    {

        currentTurn = Turn.NobodyTurn;
        gameplayAnimations.EndGameAnimation();
        TurnAllButtonsOnOff(false);
        gameIsOver = true;

    }

    //when the player wins, I display it on the screen and add another victory to his scoreboard
    void PlayerWin()
    {
        if (gameIsOver)
            return;
        Debug.Log("playerWin");
        GameOver();
        stateText.text = "YOU WIN, CONGRATULATIONS!";
        Scoreboard.AddToPlayerScore();

    }

    //when the enemy wins, I display it on the screen and add another victory to his scoreboard
    void EnemyWin()
    {
        if (gameIsOver)
            return;
        Debug.Log("enemyWin");
        GameOver();
        stateText.text = "YOU LOST, TRY AGAIN!";
        Scoreboard.AddToEnemyScore();
    }


    //when its a draw game, I display it on the screen.
    void DrawGame()
    {
        Debug.Log("Draw");
        GameOver();
        stateText.text = "WE HAVE A TIE, TRY AGAIN!";
    }

    //function called in the return button so we can go back to menu
    public void GoToMenu()
    {
        SceneManager.LoadSceneAsync("Menu");
    }

    #endregion



    //when the enemy is thinking/playing or the game is over, we don't want the player to be able to click any buttons, so this function disables all of them
    public void TurnAllButtonsOnOff(bool value)
    {
        var buttons = myBoardController.boardSpots;
        for (int i = 0; i < buttons.Count; i++)
        {
            if (buttons[i].view.myButton != null)
                buttons[i].controller.TurnOnOffButton(value);
        }
    }



}
