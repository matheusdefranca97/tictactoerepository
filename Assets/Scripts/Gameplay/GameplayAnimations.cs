using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

// -------- WORK MADE BY MATHEUS FRANÇA
public class GameplayAnimations : MonoBehaviour
{
    //reference to the "EASY/NORMAL/HARD" buttons
    [Header("State Buttons")]
    [SerializeField] List<Transform> stateButtons;
    //Gameplay gameobjects
    [Header("Gameplay Related")]
    [SerializeField] Transform board;


    [SerializeField] GameObject restartButton;

    //call the IEnumerator in a Button
    public void CallStateButtonsAnimations()
    {
        StartCoroutine(StateButtonsAnimations());
    }

    //this animation occours right after the player pick a difficult, with the state buttons dissapearing and the board showing up
    IEnumerator StateButtonsAnimations()
    {
        //i move all the state buttons out of the screen
        for (int i = 0; i < stateButtons.Count; i++)
        {
            stateButtons[i].DOLocalMoveX(2000, 0.5f);
        }
        //after i finished moving them, they get destroyed
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < stateButtons.Count; i++)
        {
            Destroy(stateButtons[i].gameObject);
        }
        //and start the board
        board.gameObject.SetActive(true);
        SizeUpBoard();
    }


    //increases the size of the tic tac toe board
    void SizeUpBoard()
    {
        board.DOScale(new Vector3(1f, 1f, 1f), 0.5f);
    }

    //animation that shows the restart Button
    public void EndGameAnimation()
    {

        restartButton.SetActive(true);
        restartButton.transform.DOLocalMove(new Vector3(0, -1490), 0.5f);
    }


}
