using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// -------- WORK MADE BY MATHEUS FRANÇA
public class EnemyApplication : MonoBehaviour
{
    [HideInInspector] public EnemyModel model;
    [HideInInspector] public EnemyView view;
    [HideInInspector] public EnemyController controller;

    private void Awake()
    {
        model = GetComponent<EnemyModel>();
        view = GetComponent<EnemyView>();
        controller = GetComponent<EnemyController>();
    }


    // Update is called once per frame
    void Update()
    {

    }


}
