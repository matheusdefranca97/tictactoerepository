using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// -------- WORK MADE BY MATHEUS FRANÇA
public class EnemyController : EnemyApplication
{
    EnemyApplication myApp;
    GameController myGameController;
    BoardController myBoardController;
    private void Awake()
    {
        myApp = GetComponent<EnemyApplication>();
        myGameController = FindObjectOfType<GameController>();
        myBoardController = FindObjectOfType<BoardController>();
    }


    //this function exists so we can call the IEnumerator "ChooseMove" on another class
    public void CallChooseMove()
    {
        myGameController.TurnAllButtonsOnOff(false);
        StartCoroutine("ChooseMove");

    }

    //chooses the Enemy move according to the game difficulty the player choose at the beginning of the game (Easy, Medium, Hard)
    IEnumerator ChooseMove()
    {

        //simmulate thinking before  enemy makes a move
        var random = Random.Range(0.25f, 3f);
        yield return new WaitForSeconds(random);

        switch (myGameController.GetCurrentGameDifficulty())
        {
            case GameDifficulty.Easy:
                RandomMove();
                break;
            case GameDifficulty.Medium:
                MediumMove();
                break;
            case GameDifficulty.Hard:
                HardMove();
                break;
        }

    }

    //function that moves the Enemy to a spot knowing the spot itself
    void Move(SpotApplication chosenSpot)
    {
        chosenSpot.controller.ConfigurateAfterClick();
        
    }
    //function that moves the Enemy to a spot, knowing only the position of that spot in the board
    void Move(int spot)
    {
        var chosenSpot = myBoardController.boardSpots[spot];
        chosenSpot.controller.ConfigurateAfterClick();
        
    }

    //Enemy choose a random number between 0/8, check if thats position is available and make enemy move there, otherwise do the same thing again recursively
    void RandomMove()
    {

        int randomSpot = Random.Range(0, 9);

        if (myBoardController.PositionIsEmpty(randomSpot))
        {
            Move(randomSpot);
            return;
        }
        else
        {
            RandomMove();
        }

    }

    //when facing a situation where the enemy himself wins in his next move, Enemy will 70% go for it
    //when facing a situation where the player will win in his next move, has a 80% chance of successfully deny that
    //otherwise, choose a random spot
    void MediumMove()
    {
        if (myBoardController.SomeoneIsCloseToWin(SpotModel.Identifier.Enemy))
        {
            if (Random.value > 0.3)
            {
                var chosenSpot = myBoardController.GetCloseToWinBoardSpot();
                Move(chosenSpot);
            }

        }
        else if (myBoardController.SomeoneIsCloseToWin(SpotModel.Identifier.Player))
        {
            if (Random.value > 0.2)
            {
                var chosenSpot = myBoardController.GetCloseToWinBoardSpot();
                Move(chosenSpot);
            }
            else
            {
                RandomMove();
            }
        }
        else
        {
            RandomMove();
        }
    }



    //when facing a situation where the enemy himself wins in his next move, Enemy will 100% go for it
    //when facing a situation where the player wins in his next move, Enemy has a 95% of sucessfully deny it.

    void HardMove()
    {

        if (myBoardController.SomeoneIsCloseToWin(SpotModel.Identifier.Enemy))
        {
            var chosenSpot = myBoardController.GetCloseToWinBoardSpot();
            Move(chosenSpot);
        }
        else if (myBoardController.SomeoneIsCloseToWin(SpotModel.Identifier.Player))
        {
            if (Random.value > 0.05)
            {
                var chosenSpot = myBoardController.GetCloseToWinBoardSpot();
                Move(chosenSpot);
            }
            else
            {
                RandomMove();
            }
        }
        else
        {
            RandomMove();
        }
    }


}
