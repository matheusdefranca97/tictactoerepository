using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ScoreboardAnimations : MonoBehaviour
{
    [Header("Player and Enemy transforms to move")]
    [SerializeField] Transform playerTextTransform;
    [SerializeField] Transform enemyTextTransform;
    // Start is called before the first frame update
    void Start()
    {
        StartAnimation();
    }

    void StartAnimation()
    {
        playerTextTransform.DOLocalMove(new Vector3(-415, 300, 0), 1f);
        enemyTextTransform.DOLocalMove(new Vector3(415, 300, 0), 1f);

    }
}
