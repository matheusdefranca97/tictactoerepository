using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// -------- WORK MADE BY MATHEUS FRANÇA
public class Scoreboard
{

    static int playerScore, enemyScore;



    public static void AddToPlayerScore()
    {
        playerScore++;
        SavePlayerData();

    }

    public static void AddToEnemyScore()
    {
        enemyScore++;
        SaveEnemyData();
    }


    //save all the player and enemy score in Unity PlayerPrefs
    public static void SavePlayerData()
    {

        if (!PlayerPrefs.HasKey("playerScore"))
            PlayerPrefs.SetInt("playerScore", playerScore);
        else
            PlayerPrefs.SetInt("playerScore", PlayerPrefs.GetInt("playerScore") + 1);


    }

    public static void SaveEnemyData()
    {
        if (!PlayerPrefs.HasKey("enemyScore"))
            PlayerPrefs.SetInt("enemyScore", enemyScore);
        else
            PlayerPrefs.SetInt("enemyScore", PlayerPrefs.GetInt("enemyScore") + 1);
    }

}
