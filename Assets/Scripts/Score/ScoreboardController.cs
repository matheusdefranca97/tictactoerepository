using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.SceneManagement;

// -------- WORK MADE BY MATHEUS FRANÇA
public class ScoreboardController : MonoBehaviour
{
    [Header("Player and Enemy texts")]
    [SerializeField] TextMeshProUGUI playerScoreText;
    [SerializeField] TextMeshProUGUI enemyScoreText;

    private void Start()
    {
        UpdatePlayerScore();
        UpdateEnemyScore();

    }

    //update the player score according to the PlayerPrefs score saved
    public void UpdatePlayerScore()
    {
        if (!PlayerPrefs.HasKey("playerScore"))
            return;


        int playerScore = PlayerPrefs.GetInt("playerScore");


        if (playerScore == 1)
            playerScoreText.text = "YOU WON " + playerScore.ToString() + " GAME";
        else
            playerScoreText.text = "YOU WON " + playerScore.ToString() + " GAMES";
    }


    ////update the enemy score according to the PlayerPrefs score saved
    public void UpdateEnemyScore()
    {
        if (!PlayerPrefs.HasKey("enemyScore"))
            return;

        int enemyScore = PlayerPrefs.GetInt("enemyScore");
        if (enemyScore == 1)
            enemyScoreText.text = "ENEMY WON " + enemyScore.ToString() + " GAME";
        else
            enemyScoreText.text = "ENEMY WON " + enemyScore.ToString() + " GAMES";
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadSceneAsync("Menu");
    }
}
